# README #

El presente proyecto se provee como Trabajo Final para la materia Web Mining. El mismo se propone investigar distintos 
clasificadores sobre artículos descargados para la web.

En el directorio *data/* pueden encontrarse el archivo *balanced-37_5K.csv.gz* de entrenamiento con 37.500 artículos 
periodísticos, y el archivo *validacion.csv.gz* con 311 para la validación. Ambos archivos se utilizan para entrenar los
modelos y luego validar los resultados a fin de realizar comparaciones. 

## Requerimientos ##
Los scripts provistos utilizan Docker para generar un ambiente homogéneo y obtener así una ejecución controlada. **Es 
requisito indispensable contar con una instalación de Docker en funcionamiento para su correcta ejecución**.


## Estructura del proyecto ###
La estructura básica del proyecto es la siguiente:
* **bin**: contiene los scripts de ejecución de entrenamiento y prueba de modelos, además de nuestro crawler.
* **data**: contiene los sets de datos del proyecto que se usaran para entrenar y validar los modelos.
* **lib**: librerías y módulos comunes reutilizables.
* **src**: nuestro código fuente, con directorios separados para las distintas aproximaciones a la resolución del
problema.
* **work**: directorio de trabajo temporal, utilizado por nuestro crawler.

## Set up del proyecto ##
* Clonar el proyecto (`git clone https://bitbucket.org/medgc/webmining.git`)
* Correr los scripts que figuran en la próxima sección.

## Scripts ###
Los scripts aquí documentados pueden encontrarse en el directorio **bin/** de nuestro proyecto.

* **crawler.sh**: Corre el crawler basado en scrapy para la descarga de los archivos de pagina12. Antes de correr este 
script se recomienda modificar en *src/crawler/crawler.py* el valor de **MAX_PAGE_HIST** para controlar la cantidad de 
páginas hacia a atrás a descargar para cada sección del diario.
 
* **linear-svc.sh**: Corre el mejor modelo obtenido de nuestras pruebas tradicionales (Bayes, SVM, etc), es decir un 
LinearSVC.
 
* **automl.sh**: Corre el mejor modelo obtenido utilizando PyiCaret, es decir un SGDClassifier.

* **cnn.sh**: Descarga los embeddings y entrena la CNN con los archivos en /data. **ATENCION**, este script requiere 
128 GB de RAM para su ejecución, se recomienda utilizar una máquina en AWS o GCP.
* **cnn-validate.sh**: Corre la red neuronal pre-entrenada sobre los datos de validación.





## Estructura del código fuente ###
* **crawler/crawler.py**: el crawler basado en scrapy.

* **tradicional/classifier.py**: pruebas con distintos clasificadores tradicionales (Bayes, SVM, etc), medición y 
comparativa de resultados (accuracy, matriz de confusión, etc.)
* **tradicional/classifier2.py**: más pruebas con distintos clasificadores tradicionales.
* **tradicional/bayes_search_svc.py**: entrenamiento y validación de modelo Bayesiano.
* **tradicional/LinearSVC.py**: entrenamiento y validación de un LinearSVC.
* **tradicional/SVC_classifier.py**: entrenamiento y validación de modelo Bayesiano.
* **tradicional/test-clasificador-datacamp.py**: pruebas con un modelo Bayes Multinomial.


* **automl/pycaret_news_total.py**: generador de los datasets de entrenamiento utilizando pycaret, y detección del mejor 
modelo utilizando TPOT.
* **automl/AUTOML_svm_optimizado_aplicado.py**: entrenamiento y validación de un SGDClassifier, el mejor modelo 
sugerido por TPOT.

* **cnn/cnn.py**: entrenamiento de la CNN.
* **cnn/cnn_validate.py**: validacion de la CNN.

* **other/vowpal-wabbit.py**: pruebas con VW - descartadas por no obtener buenos resultados.
