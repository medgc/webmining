import logging
from nltk.tokenize import TreebankWordTokenizer
import numpy as np
import urllib
import os
import sh

def download_embeddings(file, url):

    # el archivo no existe en el file system?
    if not os.path.isfile(file):
        gz_file = file + ".gz"

        # descargo el archivo del sitio remoto
        logging.info("Descargando embeddings FastText de %s (puede demorar varios minutos)" % url)
        urllib.request.urlretrieve(url, gz_file)

        # descomprimo el archivo
        logging.info("Descomprimiendo embeddings en %s", file)
        # sh.gunzip(file)

        # descartl el archivo gz
        if os.path.isfile(gz_file):
            os.unlink(file + ".gz")
        logging.info("Embeddings descargados en %s.", file)
    else:
        logging.info("Reutilizando embeddings encontrados en el archivo %s", file)


def article_to_embeddings(text, vectors, max_seq_length, embedding_dim, stopwords_set = set()):
    """
    Convierte el texto recibido en un arreglo donde cada entrada es un vector de embeddings.
    :param text: Texto a procesar.
    :param vectors: Instancia de gensim.models.KeyedVectors que permite buscar el embedding asociado a cada palabra.
    :param max_seq_length: Cantidad de embeddings que tiene que tener el arreglo retornado. Si hay más entradas que
    palabras en text se rellena con embeddings vacíos (un vector con ceros en todas sus componentes). En el caso
    contrario se trunca text.
    :param embedding_dim: Cantidad de componentes del vector de embeddings.
    :param stopwords_set: El set de StopWords a ignorar.
    :return: un arreglo de embeddings, con max_seq_length vectores de embedding_dim componentes cada uno.
    """

    # creo un arreglo con max_seq_length posiciones y en cada una de ellas, un arreglo con embedding_dim posiciones
    # para cada palabra.
    result = np.reshape(np.repeat([0.0] * embedding_dim, max_seq_length),
                             (max_seq_length, embedding_dim))

    # itero los tokens (palabras) en el articulo
    token_index = 0
    for token in article_to_embeddings.tokenizer.tokenize(text):
        try:
            if (not token in stopwords_set):
                result[token_index] = vectors.word_vec(token)
                token_index += 1
        except KeyError as e:
            logging.warning("No se encontró embedding para el token '%s' (%s)" % (token, e))


        if token_index >= max_seq_length:
            break

    return result

# Tokenizer estatico reutilizable
article_to_embeddings.tokenizer = TreebankWordTokenizer()



