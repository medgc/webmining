#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 17:37:21 2020

@author: fborghesi
"""

import scrapy
import re
import logging
from scrapy.utils.log import configure_logging 
from scrapy.exceptions import CloseSpider
import io
import os
from datetime import datetime



class Pagina12Spider(scrapy.Spider):    
 
    FIELDS = ['url', 'filename', 'section', 'title', 'summary', 'content']
 
    # patterns pre-compilados para procesar RE
    article_url_pattern = re.compile(r'https?:\/\/www\.pagina12.com.ar\/([0-9]+)-.+$')
    section_url_pattern = re.compile(r'https?:\/\/www\.pagina12.com.ar\/secciones\/(.+?)(\?page=.+)?$')
    html_tag_pattern = re.compile('<[^>]+>')

    # configuracion del crawler
    name = 'Pagina12Spider'
    page_arg = 'page'
    base_url = 'https://www.pagina12.com.ar/secciones'
    #sections = ['sociedad', 'economia', 'el-pais', 'cultura-y-espectaculos', 'el-mundo', 'deportes', 'ciencia']
    sections = ['economia', 'el-pais', 'el-mundo']

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 25,  # The maximum number of concurrent (i.e. simultaneous) requests that will be performed to any single domain.
        'DOWNLOAD_DELAY': 0.25,                # The amount of time (in secs) that the downloader should wait before downloading consecutive pages from the same website
        'AUTOTHROTTLE_ENABLED': False,
        'RANDOMIZE_DOWNLOAD_DELAY': False
    }


    def __init__(self, work_dir='work/', max_page_hist=3):
        """
        Constructor.
        :param work_dir: carpeta de trabajo.
        :param max_page_hist: cantidad de paginas hacia atras a visitar en cada
        sección.
        """
        super().__init__()
        
        self.work_dir = work_dir
        os.makedirs(self.work_dir, exist_ok = True)

        # genero las URLS a procesar inicialmente
        self.start_urls = []
        for section in self.sections:
            for page in range(max_page_hist):
                url = "%s/%s?%s=%d" % (self.base_url, section, self.page_arg, page)
                self.start_urls.append(url)
                logging.debug('Agregando URL "%s".' % url)



    def get_filename(self, url, section):
        """
        Extrae del URL el nombre del archivo donde guardar el contenido del articulo.
        :param url: El URL a procesar.
        :param section: Sección siendo procesada.
        :returns: Nombre del archivo, con formato <work_dir>/<seccion>/<id_articulo>.html
        """
        m = self.article_url_pattern.search(url)
        return os.path.join(self.work_dir, section, m.group(1) + '.html')

    def discard_html(self, text):
        """
        Elimina todos los tags HTML del texto recibido.
        :param text: El texto a procesar.
        :returns: El texto sin tags (<tag>, </tag>)
        """
        return self.html_tag_pattern.sub('', text)

    def is_article_url(self, url):
        """
        Indica si el URL corresponde a un articulo.
        :param url: El URL a procesar.
        :returns: True si tiene la forma de un articulo (https//www.pagina12.com.ar/<id_articulo>-<titulo>),
        de lo contrario False.
        """
        return self.article_url_pattern.match(url) is not None

    def is_section_url(self, url):
        """
        Indica si el URL corresponde a una sección del diario.
        :param url: El URL a procesar.
        :returns: True si tiene la forma de un articulo (https//www.pagina12.com.ar/secciones/<seccion>?page=<nro_pagina>)
        de lo contrario False.
        """
        return self.get_section(url) is not None

    def get_section(self, url):
        """"
        Extrae del URL el nombre de la sección.
        :param url: El URL a procesar.
        :returns: Nombre de la sección (economía, política, ciencia, etc.)
        """
        m = self.section_url_pattern.match(url)
        return None if m is None else m.group(1)

    # noinspection PyMethodMayBeStatic
    def save(self, filename, title, html):
        """"
        Guarda el HTML en el archivo indicado.
        :param filename: Nombre del archivo donde guardar los datos.
        :param title: Titulo del articulo.
        :param html: El contenido del articulo a guardar.
        """
        try:
            with io.open(filename, "a", encoding="utf-8") as f:
                f.write(html)
                f.close()
            logging.info('Se ha guardado el artículo "%s" en %s.' % (title, filename))
        except IOError as e:
            # si hay error detengo el spider
            raise CloseSpider('Error al generar el archivo ' + filename + ': ' + e)


    def parse_article(self, response):
        """"
        Extrae del response las secciones de interés.
        :param response: el HTTP response obtenido del web server.
        """
        logging.info('Procesando URL: %s.' % response.url)
        
        section = response.meta.get('section')
        filename = self.get_filename(response.url, section)
        
        if os.path.exists(filename):
            logging.info('El artículo ya existe en "%s".' % filename)
            return

        dirname = os.path.dirname(filename)
        os.makedirs(dirname, exist_ok = True)

        # extracción de datos del response
        article_title = response.selector.xpath("//h1[@class='article-title']/text()").get()
        article_text = " ".join(response.selector.xpath("//div[@class='article-text']/p").getall())
        article_summary = response.selector.xpath("//div[@class='article-summary']/text()").get()

        # estos campos se guardan en el CSV
        yield {
            'url': response.url,
            'filename': filename,
            'section': section,
            'title': article_title,
            'summary': article_summary,
            'content': self.discard_html(article_text)
        }
        
        # guardo el artículo en el disco
        self.save(filename, article_title, article_text)
            

    def parse_section(self, response):
        """"
        Extrae del response las referencias a los artículos de la página y los encola para su futura descarga.
        :param response: el HTTP response obtenido del web server.
        """
        result = []
        urls = response.selector.xpath('//div[@class="articles-list"]//@href').getall()

        for url in urls:
            if self.is_article_url(url):
                logging.debug('Encolando URL: %s.' % url)
                result.append(url)
                
        logging.debug('Se han encontrado %d artículos.' % len(result))
        return result


    def parse(self, response):
        """"
        Procesa una sección del diario.
        :param response: el HTTP response obtenido del web server.
        """
        logging.info('Procesando URL: %s.' % response.url)

        section = self.get_section(response.url)
        if section is not None:
            for url in self.parse_section(response):
                yield scrapy.Request(url, self.parse_article, meta = {'section': section})
        else:
            logging.error('Descartando URL: %s.' % response.url)
            
       

            
            