import pandas as pd
import numpy as np
import logging
import re
import unidecode
import socket
from nltk.corpus import stopwords

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

#
# Configuracion global
#
UNICODE_DECODE = True


def is_gce_instance():
    """
    Indica si el codigo está corriendo en una máquina de google compute
    :returns: True si el código corre en una instancia de un GCE, False si no.
    """
    result = True
    try:
        socket.getaddrinfo('metadata.google.internal', 80)
    except socket.gaierror:
        result = False

    logging.info("Verificando GCE -> %r" % result)
    return result


def load_articles(files, limit=0):
    """
    Carga articulos de una lista de archivos CSV.
    :param files: El archivo CSV a cargar.
    :param limit: Cantidad de registros a retornar por cada CSV cargado (se seleccionan aleatoriamente y el resto se
    descarta).
    :returns: Un DataFrame con las columnas section y articule.
    """


    word_pattern = re.compile('[^a-zA-ZáéíóúüñÑ ]+')

    def tile_summary_content_to_article(row):
        """
        Combina las columnas de la fila del dataframe recibido eliminando todos los simbolos y dejando solo palabras y
        espacios.
        :param row: una fila de DataFrame con las columnas 'title', 'summary', 'content'.
        :returns: Un string con las columnas combinadas.
        """

        joined = ' '.join(row.values.astype(str)).lower()
        return word_pattern.sub('', unidecode.unidecode(joined) if UNICODE_DECODE else joined)

    def load_csv(file, limit):

        """
        Carga los articulos del CSV especificado, combinando las columnas title, summary y content en una única columna
        llamada article.
        :param file: El archivo CSV a cargar.
        :param limit: Cantidad de registros a retornar (se seleccionan aleatoriamente y el resto se descarta).
        :returns: Un DataFrame con las columnas section y articule.
        """
        logging.info("Cargando articulos de %s" % file)

        # cargo todo el CSV
        tmp = pd.read_csv(file, compression='infer')

        # combino las columnas title, summary y content y elimino símbolos
        TEXT_COLS = ['title', 'summary', 'content']
        tmp['article'] = tmp[TEXT_COLS].apply(lambda row: tile_summary_content_to_article(row), axis=1)

        logging.info("%d articulos cargados de %s" % (tmp.shape[0], file))

        # hay limite?
        if limit > 0 and tmp.shape[0] > limit:
            logging.info("Eliminando %d articulos sobrantes" % (tmp.shape[0] - limit))

            # selecciono filas random
            indices = np.array(tmp.index.values)
            np.random.shuffle(indices)
            indices = indices[0:limit]

            # me quedo con las filas seleccionadas
            tmp = tmp.loc[indices]

            logging.info("%d articulos conservados" % (limit))

        return tmp[['section', 'article']]

    result = pd.DataFrame(columns=['section', 'article'])

    # cargo los articulos
    for file in files:
        tmp = load_csv(file, limit)
        result = result.append(tmp, ignore_index=True)
    logging.info("%d articulos a procesar" % tmp.shape[0])

    return result


def load_stopwords(file):
    """
    Carga la lista de stopwords de un archivo y la devuelve como un set para optimizar su tiempo de acceso.
    :param file: Archivo de donde cargar los stopwords, una palabra por file.
    :return: Python set con las palabras.
    """
    logging.info("Cargando stopwords de %s" % file)

    words = []
    with open(file, 'r') as fp:
        for w in fp:
            w = w.strip().lower()
            if len(w) > 0:
                words.append(w)

    logging.info("%d stopwords cargados." % len(words))

    nltk_stopwords = stopwords.words('spanish')
    logging.info("Agregando %d stopwords de NLTK." % len(nltk_stopwords))
    words.extend(nltk_stopwords)

    return set(words)
