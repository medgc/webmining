#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 20:53:51 2020

@author: fborghesi


Referencia: https://vowpalwabbit.org/tutorials/python_first_steps.html
"""


#%%
#
# Carga de modulos.
#
# Este script utiliza vowpal wabbit, de no estar instalado en el sitema 
# correr:
# pip install pyvw

import pandas as pd
import io
from vowpalwabbit import pyvw
import re
import unidecode
import math

#%%
#
# Carga de modulos.
#

DATA_FILE = 'work/output.csv' # directorio de descarga de archivos

#%%
#
# Lectura del CSV en un dataframe
#

df = pd.read_csv(DATA_FILE)

# genero la columna 'article' como la concatenación de titulo + bajada + contenido del articulo
text_cols = ['title', 'summary', 'content']
df['article'] = df[text_cols].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# genero una columna id para las secciones
df['section_id'] = df['section'].factorize()[0]
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)


#%%
#
# Entrenamiento del modelo
#

# maximo id de sección
category_max = df['section_id'].max()

# modelo One Against All
model = pyvw.vw("--oaa %i" % category_max, quiet = False)

# matchea sucesiones de letrs mayúsculas y minúsculas (i.e. palabras).
regex = re.compile('[^a-zA-Z]')

def vw_format_row(section_id, title, summary, content):
    """
    Genera con los datos recibidos una fila fila de entrenamiento con el formato esperado por VW.
    :param section_id: El id de la sección a la que corresponde la fila.
    :param title: El titulo de la noticia (titular).
    :param summary: El resumen de la noticia (bajada).
    :param content: El contenido de la noticia (cuerpo.)
    :returns: Una fila de entrenamiendo con la forma "<clase> |text <palabras>" donde la clase se 
    corresponde con el id  de la sección y las palabras son el contenido del articulo.
    """

    # limpio los campos (los paso a unicode, descarto simbolos, etc)
    title = regex.sub(' ', unidecode.unidecode(title))
    summary = '' if str(summary) == "nan" else regex.sub(' ', unidecode.unidecode(summary))
    content = regex.sub(' ', unidecode.unidecode(content))
    
    # concateno los textos, los paso a minusculas y descarto palabras de menos de 3 caracteres
    text = '%s %s %s' % (title, summary, content)
    text = ' '.join(re.findall('\w{3,}', text.lower()))

    return "%i |text %s" % (section_id, text)

# itero el dataset, generando las filas y formateando el modelo
for index, row in df.iterrows():

    entry = vw_format_row(row['section_id'] + 1, row['title'], row['summary'], row['content'])
    #print(entry)
    model.learn(entry)



#%%
#
# Pruebas sobre el modelo.
#

def predict(model, text):
    pred = model.predict(text) - 1
    print("%s: %s" % (id_to_section[pred], text))


predict(model, "La NASA ha descubierno un nuevo planeta")
predict(model, "El dolar blue ha incrementado su precio y la brecha ha llegado al 80 por ciento")
predict(model, "El gobernador Axel Kicillof debe lidiar con los problemas de tomas de tierras")

