#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 20:24:48 2020

@author: fborghesi

"""

#%%
#
# Carga de módulos
#
# Este script utiliza scrapy, de no estar instalado en el sitema correr:
# pip install scrapy
#
# para RS
# import os
# os.chdir('C:\\Users\\u8010026\\OneDrive - Refinitiv\\downloads\\austral\\web mining\\git\\webmining')



import scrapy
from scrapy.crawler import CrawlerProcess
from multiprocessing.context import Process
from lib.pagina12spider import Pagina12Spider
#import lib.pagina12spider as pagina12spider
from datetime import datetime
import logging
import os
import shutil
import time
import platform

#%%
#
# Configuración de variables globales
#

WORK_DIR = 'work' # directorio de descarga de archivos
MAX_PAGE_HIST = 10 # cantidad de paginas hacia atrás a visitar en cada seccion
RUN_IN_FOREGROUND = platform.system() == 'Windows' # ejecuta el proceso en foreground o en background?



#%%
#
# Limpieza de archivos de corridas previas
#
# Elimina todos los archivos generados por corridas anteriores.

if os.path.isdir(WORK_DIR):
    shutil.rmtree(WORK_DIR, ignore_errors=True)

#%%
#
# Configuracion del logging
#
# creo el directorio de trabajo antes de generar los logs
os.makedirs(WORK_DIR, exist_ok = True)

# desactivo el logger por defecto            

# todo el logging va a parar a un archivo llamado crawler-<fecha>.log dentro
# del directorio de trabajo
scrapy.utils.log.configure_logging({
        'LOG_FILE': os.path.join(WORK_DIR, 'crawler-%s.log' % datetime.now().strftime("%Y%m%d%H%M%S")),
        'LOG_ENABLED': True,
        'LOG_ENCODING': 'utf-8',
        'LOG_LEVEL': 'INFO',
        'LOG_FORMAT': '%(asctime)s - %(name)s - [%(levelname)s] - %(message)s',
        'LOG_DATEFORMAT': '%Y-%m-%d %H:%M:%S',
        'LOG_STDOUT': False,
        'LOG_SHORT_NAMES': False
    }, install_root_handler = False)
logging.getLogger('scrapy').setLevel(logging.INFO)
logging.getLogger('scrapy').propagate = False

#%%

start_time = time.time()

def handle_signal():
    logging.info('Proceso finalizado: %d segundos' % (time.time() - start_time))

#
# Inicio el crawling del sitio
# creo el crawler, configuro la salida al archivo output.csv dentro del 
# directorio de trabajo
#
def crawl():
    """
    Prepara un CrawlerProcess para hacer scraping sobre Pagina12.
    """
    process = CrawlerProcess(settings={
        'FEEDS': {
            os.path.join(WORK_DIR, 'output.csv'): {
                'format': 'csv',
                'encoding': 'utf-8',
                'fields': Pagina12Spider.FIELDS
            }
        }
    })

    process.crawl(Pagina12Spider, max_page_hist=MAX_PAGE_HIST)

    # capturo la señal de finalización
    for c in process.crawlers:
        c.signals.connect(handle_signal, signal = scrapy.signals.spider_closed)

    process.start()


if RUN_IN_FOREGROUND:
    # corro el crawler en el proceso principal
    crawl()
else:
    # corro el crawler en un proceso separado
    process = Process(target = crawl)
    process.start()
    process.join()


