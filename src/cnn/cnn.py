
#%%
#
# Carga de módulos
#
import pandas as pd
import numpy as np
import os
import logging
from gensim.models import KeyedVectors
import pathlib
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import Conv1D, GlobalMaxPooling1D, Dropout
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
from lib import util, cnn_commons
import json
import nltk
nltk.download('stopwords')
#
# Variables globales
#
# directorio donde descargar embeddings, generar redes y pesos, etc
#DATA_DIR = os.path.join('data')
DATA_DIR = os.path.join(os.getcwd(), os.path.dirname(__file__), '..', '..', 'data')

# URL de donde descargar los embeddings Fast-Text
FASTTEXT_EMBEDDINGS_URL = 'http://dcc.uchile.cl/~jperez/word-embeddings/fasttext-sbwc.vec.gz' # https://github.com/dccuchile/spanish-word-embeddings

# porcentaje a reservar para validaciones
VALIDATION_SPLIT = 0.2

# tamaño de los embeddings (cant. de dimensiones o componentes de los vectores)
EMBEDDING_DIM = 300

# cantidad de palabras a considerar de cada artículo - el exceso se trunca, la falta se rellena con ceros
MAX_SEQUENCE_LENGTH = 800


STOPWORDS_FILE = os.path.join(DATA_DIR, 'stopwords_es_sin_acentos.txt')


# valores de producción
if util.is_gce_instance():
    FASTTEXT_EMBEDDINGS_FILE = os.path.join(DATA_DIR, 'fasttext-sbwc.vec')
    INPUT_FILE = os.path.join(DATA_DIR, 'balanced-37_5K.csv.gz')

    # Archivos donde guardar la CNN y weights resultantes
    CNN_MODEL_JSON_FILE = os.path.join(DATA_DIR, 'cnn_model.json')
    CNN_MODEL_WEIGHTS_FILE = os.path.join(DATA_DIR, 'cnn_weights.h5')
    CNN_MODEL_LABELS_FILE = os.path.join(DATA_DIR, 'cnn_labels.json')

else: # valores de desarrollo
    FASTTEXT_EMBEDDINGS_FILE = os.path.join(DATA_DIR, 'fasttext-sbwc-small.vec')
    INPUT_FILE = os.path.join(DATA_DIR, 'small.csv.gz')

    # Archivos donde guardar la CNN y weights resultantes
    CNN_MODEL_JSON_FILE = os.path.join(DATA_DIR, 'cnn_model-dev.json')
    CNN_MODEL_WEIGHTS_FILE = os.path.join(DATA_DIR, 'cnn_weights-dev.h5')
    CNN_MODEL_LABELS_FILE = os.path.join(DATA_DIR, 'cnn_labels-dev.json')


#%%
#
# Descargo los embeddings de ser necesario.
# URL Descarga: https://github.com/dccuchile/spanish-word-embeddings
#
pathlib.Path(DATA_DIR).mkdir(exist_ok = True)
cnn_commons.download_embeddings(FASTTEXT_EMBEDDINGS_FILE, FASTTEXT_EMBEDDINGS_URL)




#%%
#
# Cargo el archivo de embeddings, cada entrada tiene la forma:
# <palabra> <componente0> <componente2> ... <componenteN>
# donde las componentes forman vector del embedding para la palabra en cuestión.
#
logging.info("Cargando embeddings de %s (toma varios minutos)" % FASTTEXT_EMBEDDINGS_FILE)
vectors = KeyedVectors.load_word2vec_format(FASTTEXT_EMBEDDINGS_FILE)


#%%
#
# Proceso los articulos, convierto sus tokens a vectores de embeddings.
#

# cargo los articulos
df = pd.read_csv(INPUT_FILE, compression='infer')

# creo un arreglo de etiquetas vacio
labels = np.empty(df.shape[0], dtype=object)

# creo un arreglo vacio para cada articulo, cada entrada es otro arreglo con los embeddings de las primeras
# MAX_SEQUENCE_LENGTH palabras
vectorized_data = np.empty((df.shape[0], MAX_SEQUENCE_LENGTH, EMBEDDING_DIM))


# Vector de embeddings nulos, es decir MAX_SEQUENCE_LENGTH vectores con sus 300 componentes en 0.00
ZERO_VECTOR = np.reshape(np.repeat([0.0] * EMBEDDING_DIM, MAX_SEQUENCE_LENGTH),
                             (MAX_SEQUENCE_LENGTH, EMBEDDING_DIM))

# cargo los stopwords
stopwords_set = util.load_stopwords(STOPWORDS_FILE)

# me genero un vectore de posiciones random donde colocar las salidas
target_indices = np.asarray(range(df.shape[0]))
np.random.shuffle(target_indices)

# itero los articulos
for i, row in df.iterrows():
    logging.info("Procesando el articulo # %d'" % i)

    vecs = cnn_commons.article_to_embeddings(str(row['article']), vectors, MAX_SEQUENCE_LENGTH, EMBEDDING_DIM, stopwords_set)

    # guardo el label y el vector de embeddings en una posición random de sus respectivos arreglos
    target_index = target_indices[i]
    labels[target_index] = row['section']
    vectorized_data[target_index] = vecs

logging.info('%d articulos convertidos a embeddings' % (vectorized_data.shape[0]))

#%%
#
# Hacemos Encoding Binario sobre los labels
#
logging.info('Realizando encoding binario sobre los labels.')
encoder = LabelEncoder()
labels = encoder.fit_transform(labels)
with open(CNN_MODEL_LABELS_FILE, 'w') as fp:
    json.dump(encoder.classes_.tolist(), fp)



labels = np_utils.to_categorical(labels)
output_size = labels.shape[1]

logging.info('Entradas del Tensor de datos: (%d)' % (vectorized_data.shape[0]))
logging.info('Entradas del vector de etiquetas: (%d, %d)' % (labels.shape[0], labels.shape[1]))



#%%
#
# Preparo los conjuntos de train y test
#
logging.info('Separando vectorized_data en train y validación...')
nb_validation_samples = int(VALIDATION_SPLIT * len(vectorized_data))
x_train = vectorized_data[:-nb_validation_samples]
y_train = labels[:-nb_validation_samples]
x_val = vectorized_data[-nb_validation_samples:]
y_val = labels[-nb_validation_samples:]



#%%
#
# Hiperparámetros para el modelo
#
BATCH_SIZE = 32 # cantidad de muestras que la red debe ver antes de hacer back-propagation del error y actualizar los weights
FILTERS = 250 # cantidad de filtros a entrenar
KERNEL_SIZE = 3 # tamaño de los filtros (equivale a ngrams en el esquema tradicional)
HIDDEN_DIMS = 450 # tamaño de la hidden layer
EPOCHS = 16 # cantidad de veces que se recorre el dataset
DROP_OUT = 0.2 # cantidad de embeddings que NO pasan a la siguiente capa

logging.info(
    'Creando el modelo (Batch Size: %d, Filters: %d, Kernel Size: %d, Hidden Dims: %d, Epochs: %d, Drop Out: %d)' %
    (BATCH_SIZE, FILTERS, KERNEL_SIZE, HIDDEN_DIMS, EPOCHS, DROP_OUT)
)

model = Sequential()

# Aplicamos un filtro a cada articulo, obteniendo vectores 1D
model.add(Conv1D(FILTERS,
                 KERNEL_SIZE,
                 padding='valid',
                 activation='relu',
                 # activation='softmax',
                 strides=1,
                 input_shape=(MAX_SEQUENCE_LENGTH, EMBEDDING_DIM)))

# Usamos Global Max Pooling 1D para quedarnos con el máximo de cada filtro
model.add(GlobalMaxPooling1D())

# Agregamos la capa oculta
model.add(Dense(HIDDEN_DIMS))

# Ponemos una capa oculta para que (1-DROP_OUT)% de los embeddings pasen a la capa siguiente
model.add(Dropout(DROP_OUT))

# Aplicamos RELU a la salida de cada neurona
model.add(Activation('relu'))

# Nuestra output layer que sería nuestro clasificador
model.add(Dense(output_size))
#model.add(Activation('sigmoid'))
model.add(Activation('softmax'))


# Entrenamos el modelo
logging.info("Compilando el modelo.")
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.fit(x_train, y_train,
              batch_size=BATCH_SIZE,
              epochs=EPOCHS,
              validation_data=(x_val, y_val))


#TEXT = "alberto fernandez la unidad de todos es importantisima para que la argentina levante cabeza el plan contempla la asistencia directa a intendencias por  millones de pesos para impulsar la economia el presidente alberto fernandez encabezo esta tarde desde la residencia de olivos la presentacion del programa federal municipios de pie que contempla la asistencia directa a intendencias por  millones de pesos para impulsar la economia la integracion regional y la inclusion social en todo el territorio nacional nos ha tocado gobernar en un tiempo muy dificil donde la unidad de todos nosotros es importantisima para que la argentina levante cabeza afirmo el presidente ante  intendentes e intendentas de todo el pais que se comunicaron por videoconferencia acompanado por el ministro del interior eduardo de pedro destaco no me importa de donde vienen si son radicales si son socialistas si son peronistas o son independientes o son cambiemos lo unico que quiero saber es si quieren el mismo pais que yo que es un pais mas justo mas solidario y mas integrado si es asi abracemonos y caminemos juntos fernandez explico que el programa tiene como ultimo proposito ayudar a los intendentes a gobernar mejor sus ciudades y sus pueblos que puedan llegar los recursos directamente a esos municipios que puedan disponer de esos recursos para las cosas que mas necesitan y puedan resolverlas  a ustedes este plan les esta cambiando la vida y eso es lo que le plantee a wado anda a preguntar a cada municipio que es lo que necesitan como podemos ayudarlos a concretar sus urgencias inmediatas sostuvo el mandatario ademas destaco la tarea de los intendentes porque cuando un vecino tiene un problema no viene a golpear la puerta del presidente va a golpear la puerta del intendente y es al intendente al que le piden la solucion de los problemas mas inmediatos por su parte el ministro del interior remarco que este programa busca unir el proyecto federal con necesidades concretas y especificas que cada municipio tiene el proyecto de una argentina federal tiene que ver con una argentina que de igualdad de oportunidades en cada uno de los rincones de nuestro pais y esas oportunidades tienen que ver generalmente con poder acceder a un trabajo digno a una vivienda digna y a una educacion digna expreso participaron tambien de la presentacion del programa el secretario de municipios avelino zurro el subsecretario de relaciones municipales pablo giles la directora de asuntos municipales johanna fare y la directora del observatorio municipal mariana alcoba la iniciativa articulada a traves de la secretaria de municipios y la subsecretaria de relaciones municipales de la cartera de interior brinda asistencia tecnica y financiera para la adquisicion de equipamiento insumos y otros bienes de capital para potenciar el rol de cada comuna los gobiernos locales que forman parte del programa fueron seleccionados de acuerdo a una previa construccion del indice federal de inequidad territorial ifit e incluyen  municipios de la provincia de buenos aires  de la zona centro del pais  de la region cuyana  del noreste  del noroeste  de patagonia norte y  de patagonia sur pertenecen a las provincias de buenos aires  cordoba  entre rios  santa fe  la rioja  san juan  mendoza  san luis  chaco  corrientes  catamarca  jujuy  salta  tucuman  la pampa  neuquen  rio negro  chubut  y tierra del fuego "
#model.predict(np.asarray(
# [cnn_commons.article_to_embeddings(TEXT, vectors, MAX_SEQUENCE_LENGTH, EMBEDDING_DIM)]
#))


# Guardamos el modelo
logging.info("Guardando el modelo en %s", CNN_MODEL_JSON_FILE)
model_structure = model.to_json()
with open(CNN_MODEL_JSON_FILE, "w") as json_file:
    json_file.write(model_structure)

logging.info("Guardando weights en %s", CNN_MODEL_WEIGHTS_FILE)
model.save_weights(CNN_MODEL_WEIGHTS_FILE)
print('Model saved.')

