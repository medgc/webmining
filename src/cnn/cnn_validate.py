#%%
#
# Carga de módulos
#
import pandas as pd
import numpy as np
import os
import logging
from gensim.models import KeyedVectors
import pathlib
from lib import cnn_commons
from lib import util
from keras.models import model_from_json
from sklearn.metrics import confusion_matrix
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
import json
import matplotlib.pyplot as plt

#%%
#
# Variables globales
#

# directorio donde descargar embeddings, generar redes y pesos, etc
DATA_DIR = os.path.join(os.path.dirname(__file__), '..', '..', 'data')

# URL de donde descargar los embeddings Fast-Text
FASTTEXT_EMBEDDINGS_URL = 'http://dcc.uchile.cl/~jperez/word-embeddings/fasttext-sbwc.vec.gz' # https://github.com/dccuchile/spanish-word-embeddings

# Archivos donde leer la CNN y weights resultantes
CNN_MODEL_JSON_FILE = os.path.join(DATA_DIR, 'cnn_model.json')
CNN_MODEL_WEIGHTS_FILE = os.path.join(DATA_DIR, 'cnn_weights.h5')
CNN_MODEL_LABELS_FILE = os.path.join(DATA_DIR, 'cnn_labels.json')

# Dataset de validacion
INPUT_CSV = os.path.join(DATA_DIR, 'validacion.csv.gz')

# tamaño de los embeddings (cant. de dimensiones o componentes de los vectores)
EMBEDDING_DIM = 300

# cantidad de palabras a considerar de cada artículo - el exceso se trunca, la falta se rellena con ceros
MAX_SEQUENCE_LENGTH = 800

# valores de producción
if util.is_gce_instance():
    FASTTEXT_EMBEDDINGS_FILE = os.path.join(DATA_DIR, 'fasttext-sbwc.vec')
else: # valores de desarrollo
    #FASTTEXT_EMBEDDINGS_FILE = os.path.join(DATA_DIR, 'fasttext-sbwc-small.vec')
    FASTTEXT_EMBEDDINGS_FILE = os.path.join(DATA_DIR, 'fasttext-sbwc.vec')



#%%
#
# Embeddings
#

# descargo los embeddings de la web si es necesario
pathlib.Path(DATA_DIR).mkdir(exist_ok = True)
cnn_commons.download_embeddings(FASTTEXT_EMBEDDINGS_FILE, FASTTEXT_EMBEDDINGS_URL)

# levanto los embeddings a memoria
logging.info("Cargando embeddings de %s (toma varios minutos)" % FASTTEXT_EMBEDDINGS_FILE)
vectors = KeyedVectors.load_word2vec_format(FASTTEXT_EMBEDDINGS_FILE)


#%%
#
# Modelo
#

# cargo el JSON del modelo
logging.info('Cargando modelo de "%s".' % CNN_MODEL_JSON_FILE)
json_file = open(CNN_MODEL_JSON_FILE, 'r')
loaded_model_json = json_file.read()
json_file.close()

# instancio el modelo
logging.info('Creando el modelo a partir de los datos cargados".')
model = model_from_json(loaded_model_json)

# cargo los weights
logging.info('Cargando weights de "%s".' % CNN_MODEL_WEIGHTS_FILE)
model.load_weights(CNN_MODEL_WEIGHTS_FILE)

# compilo el modelo
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])




#%%
#
# Cargo el archivo de embeddings, cada entrada tiene la forma:
# <palabra> <componente0> <componente2> ... <componenteN>
# donde las componentes forman vector del embedding para la palabra en cuestión.
#
logging.info("Cargando embeddings de %s (toma varios minutos)" % FASTTEXT_EMBEDDINGS_FILE)
vectors = KeyedVectors.load_word2vec_format(FASTTEXT_EMBEDDINGS_FILE)




#%%
#
# Carga de datos de validacion
#
logging.info('Cargando dataset de "%s".' % INPUT_CSV)
df = pd.read_csv(INPUT_CSV, compression='gzip')

X = np.empty((df.shape[0], MAX_SEQUENCE_LENGTH, EMBEDDING_DIM))
labels = np.empty(df.shape[0], dtype=object)

for i, row in df.iterrows():
    logging.info("Procesando el articulo # %d'" % i)

    vecs = cnn_commons.article_to_embeddings(str(row['article']), vectors, MAX_SEQUENCE_LENGTH, EMBEDDING_DIM)

    # guardo el label y el vector de embeddings en una posición random de sus respectivos arreglos
    labels[i] = row['section']
    X[i] = vecs

logging.info('%d articulos convertidos a embeddings' % (X.shape[0]))


logging.info('Cargando las categorías de %s.' % (CNN_MODEL_LABELS_FILE))
with open(CNN_MODEL_LABELS_FILE, 'r') as fp:
    categories = json.load(fp)


encoder = LabelEncoder()
labels = encoder.fit_transform(labels)
y = np_utils.to_categorical(labels)



#%%
#
# Evaluación del modelo
#
score = model.evaluate(X, y, verbose=0)
logging.info('%s: %.2f%%' % (model.metrics_names[1], score[1] * 100))

y_pred = model.predict(X)
conf_mat = confusion_matrix(list(map(lambda x: np.argmax(x), y)), list(map(lambda x: np.argmax(x), y_pred)))
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=categories, yticklabels=categories)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()
