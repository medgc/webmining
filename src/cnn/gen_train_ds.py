"""
Carga 12.500 (ARTICLE_LOAD_LIMIT) artículos por cada sección del diario (economía, el-mundo y el-pais) y genera un
archivo de salida (OUTPUT_FILE) combinando los mismos.
"""
from lib import util
import os
import logging

WORK_DIR = os.path.join('work')
INPUT_FILES = list(map(lambda file: os.path.join(WORK_DIR, file), ['economia.csv', 'el-mundo.csv', 'el-pais.csv']))
OUTPUT_FILE = os.path.join(WORK_DIR, 'validation.csv')

#INPUT_FILES = list(map(lambda file: os.path.join(WORK_DIR, file), ['small.csv']))
#OUTPUT_FILE = os.path.join(WORK_DIR, 'validation.csv')

ARTICLE_LOAD_LIMIT = 12500

df = util.load_articles(INPUT_FILES, ARTICLE_LOAD_LIMIT)

logging.info("Generando el archivo %s con %d articulos." % (OUTPUT_FILE, df.shape[0]))
df.to_csv(OUTPUT_FILE, index=False)

logging.info("Archivo generado.")
