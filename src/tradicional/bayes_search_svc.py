# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 15:09:21 2020

@author: U8010026
"""

import re
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
#from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import seaborn as sns
import matplotlib.pyplot as plt
#from IPython.display import display
from sklearn import metrics
import os
import joblib
from typing import Pattern, Optional, List, Tuple, Callable


#%%

def leer_archivo(path:str) -> str:
    return open(path,"rt", encoding='utf-8').read()

def leer_stopwords(path:str) -> List[str]:
    with open(path,"rt") as stopwords_file:
        return [stopword for stopword in [stopword.strip().lower() for stopword in stopwords_file] if len(stopword)>0 ]

def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str],List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    if token_regex is None:
        # definicion de que es un token: una letra seguida de letras y numeros
        token_regex = r"[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+"
        token_pattern = re.compile(token_regex)
    return lambda doc: token_pattern.findall(doc)

STOPWORDS_FILE = "stopwords_es.txt"
STOPWORDS_FILE_SIN_ACENTOS = "stopwords_es_sin_acentos.txt"




#%%
#
# Descargo los stopwords con NLTK
#

nltk.download('stopwords')

#%%
#
# Descargo los stopwords con NLTK
#

DATA_FILE = 'work/balanced-37_5K.csv' # directorio de descarga de archivos
VALIDACION='work/validation.csv'
TEST_SIZE = 0.3               # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

MIN_DF=3
# cantidad maxima de docs que tienen que tener a un token para conservarlo.
MAX_DF=0.8

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2

mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE_SIN_ACENTOS)
mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))
#mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE)+mi_lista_stopwords
#mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))

#%%

#
# Carga del archivo de datos
#

# lectura del CSV en un dataframe
df1 = pd.read_csv(DATA_FILE)
#df = ultil.load_articles(['/work/economia.csv.gz', '/work/el-mundo.csv.gz', '/work/el-pais.csv.gz'], 400)
N=5000
df=df1.groupby('section').apply(lambda x: x[:N][['section','article']])
#df=df1
df['section_id'] = df['section'].factorize()[0]
# genero la columna 'article' como la concatenación de titulo + bajada + contenido del articulo
#text_cols = ['title', 'summary', 'content']
#text_cols = ['title', 'summary', 'content']

#df['article'] = df[text_cols].apply(lambda row: re.sub('[^a-zA-ZáéíóúüñÑ ]+|nan', '', ' '.join(row.values.astype(str))), axis=1)


tokenizer = tokenizador()

# creo el vectorizador
tfidf_vect = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS), # uni-gramas y bi-gramas parecen razonables
    lowercase=True,
    stop_words=mi_lista_stopwords,
    tokenizer=tokenizer,
    #stop_words = stopwords.words('spanish')# usamos los stopwords de NLTK
    )
# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)
# procesamos los artículos
#features = tfidf_vect.fit_transform(df.article).toarray()
labels = df.section_id


#%%
#
# Generacion de datasets de train y test.
#

X = df['article']
y = df['section']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state = 42)



#%%
#
# 
X_train_tfidf = tfidf_vect.fit_transform(X_train)
X_test_tfidf = tfidf_vect.transform(X_test)
X_train=X_train_tfidf.A
X_test=X_test_tfidf.A
import skopt
print('skopt %s' % skopt.__version__)
from skopt import BayesSearchCV
from sklearn.datasets import load_digits
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold

#X, y = load_digits(n_class=10, return_X_y=True)
#X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.75, test_size=.25, random_state=0)
# log-uniform: understand as search over p = exp(x) by varying x
opt = BayesSearchCV(
SVC(),
{
'C': (1e-6, 1e+6, 'log-uniform'),
'gamma': (1e-6, 1e+1, 'log-uniform'),
'degree': (1, 8), # integer valued parameter
'kernel': ['linear', 'poly', 'rbf','sigmoid'], # categorical parameter
},
n_iter=32,
cv=3
)
opt.fit(X_train, y_train)
#opt.fit(X_train_tfidf.A, y_train)
print("val. score: %s" % opt.best_score_)
print("test score: %s" % opt.score(X_test_tfidf, y_test))
print("mejores parametros: %s" % opt.best_params_)



#%%
#model = LinearSVC()
model=SVC(C=6027.646962626502,degree= 6, gamma= 0.018856829968386307,kernel='rbf')
#model = LogisticRegression(random_state=0)
#X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
from sklearn.metrics import confusion_matrix
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)


#%%
from sklearn.pipeline import Pipeline

# pipeline class is used as estimator to enable
# search over different model types
pipe = Pipeline([
    ('model',LinearSVC())
])

# single categorical value of 'model' parameter is
# sets the model class
# We will get ConvergenceWarnings because the problem is not well-conditioned.
# But that's fine, this is just an example.
linsvc_search = {
    'model': [LinearSVC(max_iter=1000)],
    'model__C': (1e-6, 1e+6, 'log-uniform'),
}

# explicit dimension classes can be specified like this


opt2 = BayesSearchCV(
    pipe,
    # (parameter space, # of evaluations)
    [(linsvc_search, 16)],
    cv=3
)

opt2.fit(X_train, y_train)
print("val. score: %s" % opt2.best_score_)
print("test score: %s" % opt2.score(X_test_tfidf, y_test))
print("mejores parametros: %s" % opt2.best_params_)


#%%
model = LinearSVC(C=24.60751195696892)
#model = LogisticRegression(random_state=0)
#X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33)
#, random_state=0)

#X_train=X_train_tfidf
#X_test=X_test_tfidf
model.fit(X_train, y_train)

filename = 'LinearSVC.sav'
joblib.dump(model, filename)

y_pred = model.predict(X_test)
from sklearn.metrics import confusion_matrix
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)

val_df = pd.read_csv(VALIDACION)
X_val=tfidf_vect.transform(val_df['article']).A
y_pred = model.predict(X_val)
y_test=val_df['section']
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)




