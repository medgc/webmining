# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 19:22:58 2020

@author: U8010026
"""
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 15:09:21 2020

@author: U8010026
"""

import re
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
#from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import seaborn as sns
import matplotlib.pyplot as plt
#from IPython.display import display
from sklearn import metrics
import os
import joblib
from typing import Pattern, Optional, List, Tuple, Callable


#%%

def leer_archivo(path:str) -> str:
    return open(path,"rt", encoding='utf-8').read()

def leer_stopwords(path:str) -> List[str]:
    with open(path,"rt") as stopwords_file:
        return [stopword for stopword in [stopword.strip().lower() for stopword in stopwords_file] if len(stopword)>0 ]

def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str],List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    if token_regex is None:
        # definicion de que es un token: una letra seguida de letras y numeros
        token_regex = r"[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+"
        token_pattern = re.compile(token_regex)
    return lambda doc: token_pattern.findall(doc)

STOPWORDS_FILE = "stopwords_es.txt"
STOPWORDS_FILE_SIN_ACENTOS = "stopwords_es_sin_acentos.txt"




#%%
#
# Descargo los stopwords con NLTK
#

nltk.download('stopwords')

#%%
#
# Descargo los stopwords con NLTK
#

DATA_FILE = 'data/balanced-37_5K.csv.gz' # directorio de descarga de archivos
TEST_SIZE = 0.3               # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

MIN_DF=3
# cantidad maxima de docs que tienen que tener a un token para conservarlo.
MAX_DF=0.8

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2

mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE_SIN_ACENTOS)
mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))
#mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE)+mi_lista_stopwords
#mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))

#%%

#
# Carga del archivo de datos
#

# lectura del CSV en un dataframe
df = pd.read_csv(DATA_FILE)
#df = ultil.load_articles(['/work/economia.csv.gz', '/work/el-mundo.csv.gz', '/work/el-pais.csv.gz'], 400)
df['section_id'] = df['section'].factorize()[0]

#df['article'] = df[text_cols].apply(lambda row: re.sub('[^a-zA-ZáéíóúüñÑ ]+|nan', '', ' '.join(row.values.astype(str))), axis=1)


tokenizer = tokenizador()

# creo el vectorizador
tfidf_vect = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS), # uni-gramas y bi-gramas parecen razonables
    lowercase=True,
    stop_words=mi_lista_stopwords,
    tokenizer=tokenizer,
    #stop_words = stopwords.words('spanish')# usamos los stopwords de NLTK
    )
# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)
# procesamos los artículos
#features = tfidf_vect.fit_transform(df.article).toarray()
labels = df.section_id


#%%
#
# Generacion de datasets de train y test.
#

X = df['article']
y = df['section']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state = 42)



#%%
#
# 
X_train_tfidf = tfidf_vect.fit_transform(X_train)
X_test_tfidf = tfidf_vect.transform(X_test)
X_train=X_train_tfidf.A
X_test=X_test_tfidf.A

#%%
#model = LinearSVC()
model=SVC(C=6027.646962626502,degree= 6, gamma= 0.018856829968386307,kernel='rbf')
#model = LogisticRegression(random_state=0)
#X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
from sklearn.metrics import confusion_matrix
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)




