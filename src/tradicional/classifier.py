#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 20:24:48 2020

@author: fborghesi


#%% 
#
# Carga de módulos
#
# Este script utiliza scikit-learn, seaborn y nltk, de no estar instalado en el sitema 
# correr:
# pip install sklearn seaborn nltk
#
"""


import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
#from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import seaborn as sns
import matplotlib.pyplot as plt
import os
from sklearn import metrics
import re
import unidecode
from sklearn.metrics import confusion_matrix
import util

#%%
#
# Descargo los stopwords con NLTK
#

nltk.download('stopwords')

#%%
#
# Descargo los stopwords con NLTK
#
DATA_DIR = os.path.join(os.getcwd(), os.path.dirname(__file__), '..', '..', 'data')
INPUT_CSV = os.path.join(DATA_DIR, 'balanced-37_5K.csv.gz') # Archivos de datos


TEST_SIZE = 0.3               # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

# cantidad minima de docs que tienen que tener a un token para conservarlo.
MIN_DF=3
# cantidad maxima de docs que tienen que tener a un token para conservarlo.
MAX_DF=0.8

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2


#%%
#
# Carga del archivo de datos
#

# lectura del CSV en un dataframe
df = pd.read_csv(INPUT_CSV, compression='infer')

# genero una columna id para las secciones
df['section_id'] = df['section'].factorize()[0]

# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)



#%%
#
# Procesamiento TF-IDF (conversión a vectores)
#
# Consideramos cada artículo como un "bag of words", es decir consideramos la 
# presencia de las palabras y su frecuencia, pero no su orden.
#

# creo el vectorizador
tfidf = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df = 5,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range = (1, 2),                   # uni-gramas y bi-gramas parecen razonables
    stop_words = stopwords.words('spanish'), # usamos los stopwords de NLTK
    decode_error='ignore'
)

count_vect = CountVectorizer(
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS),                   # uni-gramas y bi-gramas parecen razonables
    stop_words = stopwords.words('spanish'), # usamos los stopwords de NLTK
    lowercase=True,
    strip_accents='unicode',
    decode_error='ignore',
    max_df=MAX_DF
)
# procesamos los artículos
features = tfidf.fit_transform(df.article).toarray()
labels = df.section_id

features.shape  # (cantidad de articulos, cantidad de features)


#%%
#
# Usamos Chi^2 para encontrar los términos más correlacionados con cada sección 
# del diario.
#

def imprimir_resumen(seccion, unigramas, bigramas):
    TOP_N = 5

    titulo = "Top %d de la sección '%s'" % (TOP_N, seccion)
    print(titulo)
    print('-' * len(titulo))

    print("Uni-gramas más correlacionados: ")
    for u in unigramas[-TOP_N:]:
        print(" * %s" % u)

    print("\nBi-gramas más correlacionados: ")
    for b in bigramas[-TOP_N:]:
        print(" * %s" % b)

    print("\n")


def encontrar_n_gramas(array, n):
    result = []

    for w in array:
        if w.count(' ') == n - 1:
            result.append(w)

    return result


for section, section_id in sorted(section_to_id.items()):
    features_chi2 = chi2(features, labels == section_id)
    indices = np.argsort(features_chi2[0])
    feature_names = np.array(tfidf.get_feature_names())[indices]

    unigrams = encontrar_n_gramas(feature_names, 1)
    bigrams = encontrar_n_gramas(feature_names, 2)

    imprimir_resumen(section, unigrams, bigrams)


#%%
#
# Generacion de datasets de train y test.
#

X = df['article']
y = df['section']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state = 42)



#%%
#
# Prueba de modelo Naive Bayes.
#

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
clf = MultinomialNB().fit(X_train_tfidf, y_train)

# pruebo con un par de noticias inventadas
print(clf.predict(count_vect.transform(["La NASA ha descubierno un nuevo planeta"])))
print(clf.predict(count_vect.transform(["El dolar blue ha incrementado su precio y la brecha ha llegado al 80 por ciento"])))
print(clf.predict(count_vect.transform(["El gobernador Axel Kicillof debe lidiar con los problemas de tomas de tierras"])))


#%%
#
# Benchmark de modelos:
# Random Forest vs SVM (LinearSVC) vs MultinomialNB (Naive Bayes) vs Logistic 
# Regression.
#

models = [
    RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0),
    LinearSVC(),
    MultinomialNB(),
    LogisticRegression(random_state=0),
]

CV = 5
cv_df = pd.DataFrame(index=range(CV * len(models)))

entries = []
for model in models:
  model_name = model.__class__.__name__
  accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
  for fold_idx, accuracy in enumerate(accuracies):
    entries.append((model_name, fold_idx, accuracy))

cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])

sns.boxplot(x='model_name', y='accuracy', data=cv_df)
sns.stripplot(x='model_name', y='accuracy', data=cv_df, size=5, jitter=True, edgecolor="gray", linewidth=2)
plt.show()

#%%
#
# Prueba de modelo SVM
# 

model = LinearSVC()
#model = LogisticRegression(random_state=0)
X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)

y_pred = model.predict(X_test)

conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

#%%
#
# Verificación de errores
#
# Verificamos los errores de cada sección mal clasificada para entender que pasó.
#

def mostrar_errores(predicted, actual, cant_errores, df):
  header = "'%s' predicted as '%s' : %s examples:" % (id_to_section[actual], id_to_section[predicted], conf_mat[actual, predicted])
  print(header)
  print('-' * len(header))
  print(df[['section', 'article']])
  print('')


example_count = 2 # cantidad de errores de clasificación mínimo, para evitar mostrar demasiada data

for predicted in section_id_df.section_id:
  for actual in section_id_df.section_id:
    if predicted != actual and conf_mat[actual, predicted] >= example_count:
      mostrar_errores(
        predicted, 
        actual, 
        conf_mat[actual, predicted], 
        df.loc[indices_test[(y_test == actual) & (y_pred == predicted)]]
      )




#%%
#
# Generamos un reporte de clasificación por cada clase
#
# $Precision=\frac{True Positive}{True Positive + False Positive}$
#
# $Recall=\frac{True Positive}{True Positive + False Negative}$
# 
# $F1=\frac{Precision * Recall}{Precision + Recall}$
# 
# $Accuracy=\frac{True Positives + True Negatives}{True Positives + True Negatives + 
# 
# False Positives + False Negatives}$
# 

print(metrics.classification_report(y_test, y_pred, target_names=df['section'].unique()))











