# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 10:10:29 2020

@author: U8010026
"""
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 15:09:21 2020

@author: U8010026
"""

import re
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from sklearn.metrics import accuracy_score
from nltk.corpus import stopwords
from sklearn.svm import LinearSVC
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import metrics
import os
from typing import Pattern, Optional, List, Tuple, Callable
from sklearn.metrics import confusion_matrix


#%%


DATA_DIR = os.path.join(os.getcwd(), os.path.dirname(__file__), '..', '..', 'data')

def leer_archivo(path:str) -> str:
    return open(path,"rt", encoding='utf-8').read()

def leer_stopwords(path:str) -> List[str]:
    with open(path,"rt") as stopwords_file:
        return [stopword for stopword in [stopword.strip().lower() for stopword in stopwords_file] if len(stopword)>0 ]

def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str],List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    if token_regex is None:
        # definicion de que es un token: una letra seguida de letras y numeros
        token_regex = r"[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+"
        token_pattern = re.compile(token_regex)
    return lambda doc: token_pattern.findall(doc)



STOPWORDS_FILE = os.path.join(DATA_DIR, "stopwords_es.txt")
STOPWORDS_FILE_SIN_ACENTOS = os.path.join(DATA_DIR, "stopwords_es_sin_acentos.txt")




#%%
#
# Descargo los stopwords con NLTK
#

nltk.download('stopwords')

#%%
#
# Descargo los stopwords con NLTK
#

DATA_FILE = os.path.join(DATA_DIR, "balanced-37_5K.csv.gz") # directorio de descarga de archivos
VALIDACION= os.path.join(DATA_DIR, "validacion.csv.gz")
TEST_SIZE = 0.3               # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

MIN_DF=3
# cantidad maxima de docs que tienen que tener a un token para conservarlo.
MAX_DF=0.8

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2

mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE_SIN_ACENTOS)
mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))


#%%

#
# Carga del archivo de datos
#

# lectura del CSV en un dataframe
df1 = pd.read_csv(DATA_FILE, compression='infer')

#---- si quiero probar entrenar con menos ---
#df = ultil.load_articles(['/work/economia.csv.gz', '/work/el-mundo.csv.gz', '/work/el-pais.csv.gz'], 400)
#N=12000
#df=df1.groupby('section').apply(lambda x: x[:N][['section','article']])


df=df1
df['section_id'] = df['section'].factorize()[0]


tokenizer = tokenizador()

# creo el vectorizador
tfidf_vect = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS), # uni-gramas y bi-gramas parecen razonables
    lowercase=True,
    stop_words=mi_lista_stopwords,
    tokenizer=tokenizer
    )

# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)
# procesamos los artículos
#features = tfidf_vect.fit_transform(df.article).toarray()
labels = df.section_id


#%%
#
# Generacion de datasets de train y test.
#

X = df['article']
y = df['section']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state = 42)



#%%
#
# 
X_train_tfidf = tfidf_vect.fit_transform(X_train)
X_test_tfidf = tfidf_vect.transform(X_test)
X_train=X_train_tfidf
X_test=X_test_tfidf

#%%

#luego de correr un optimizador bayesiano, este es el mejor hiperparametro
model = LinearSVC()
    #C=24.60751195696892)

model.fit(X_train, y_train)

filename = 'LinearSVC.sav'
#joblib.dump(model, filename)


y_pred = model.predict(X_test)

print("Accuracy en Test: %f" % accuracy_score(y_test, y_pred))


"""
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()
"""

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)

df_holdout = pd.read_csv(VALIDACION, compression='infer')
df_holdout['section_id'] = list(map(lambda section: section_to_id[section], df_holdout['section']))

X_houldout = tfidf_vect.transform(df_holdout['article'])
y_holdout =  df_holdout['section']
y_holdout_pred = model.predict(X_houldout)
print("Accuracy en Validacion: %f" % accuracy_score(y_holdout, y_holdout_pred))


conf_mat = confusion_matrix(y_holdout, y_holdout_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_holdout, y_holdout_pred)
print(cm_svc)




