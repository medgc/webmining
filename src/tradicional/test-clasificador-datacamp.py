# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 16:19:18 2020

@author: U8010026
"""
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.model_selection import train_test_split

# Create a series to store the labels: y
y = df['section']

spanish_stop_words = stopwords.words('spanish')

# Create training and test sets
X_train, X_test, y_train, y_test = train_test_split(df['article'],y,test_size=0.30)


#%%

#### armo vectores de countvectorizer #######

# Initialize a CountVectorizer object: count_vectorizer
count_vectorizer = CountVectorizer(stop_words=spanish_stop_words)
# Transform the training data using only the 'text' column values: count_train 
count_train = count_vectorizer.fit_transform(X_train.values)
# Transform the test data using only the 'text' column values: count_test 
count_test = count_vectorizer.transform(X_test.values)
# Print the first 10 features of the count_vectorizer
print(count_vectorizer.get_feature_names()[:10])

#%%

#### armo vectores de tfidf vectorizer #######

# Initialize a TfidfVectorizer object: tfidf_vectorizer
tfidf_vectorizer = TfidfVectorizer(stop_words=spanish_stop_words, max_df=1)
# Transform the training data: tfidf_train 
tfidf_train = tfidf_vectorizer.fit_transform(X_train)
# Transform the test data: tfidf_test 
tfidf_test = tfidf_vectorizer.transform(X_test)
# Print the first 10 features
print(tfidf_vectorizer.get_feature_names()[:10])
# Print the first 5 vectors of the tfidf training data
print(tfidf_train.A[:5])



#%%
### predecir con countvectorizer

# Instantiate a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier = MultinomialNB()
# Fit the classifier to the training data
nb_classifier.fit(count_train, y_train)
# Create the predicted tags: pred
pred = nb_classifier.predict(count_test)
# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test, pred)
print(score)
# Calculate the confusion matrix: cm
cm = metrics.confusion_matrix(y_test, pred)
print(cm)

#%%
### predecir con tfidf vectorizer

# Instantiate a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier_tf = MultinomialNB()
# Fit the classifier to the training data
nb_classifier_tf.fit(tfidf_train, y_train)
# Create the predicted tags: pred
tfidf_pred = nb_classifier_tf.predict(tfidf_test)
# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test, tfidf_pred)
print(score)
# Calculate the confusion matrix: cm
cm = metrics.confusion_matrix(y_test, tfidf_pred)
print(cm)


