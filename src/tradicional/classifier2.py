# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 18:08:23 2020

@author: U8010026
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 20:24:48 2020

@author: fborghesi


#%% 
#
# Carga de módulos
#
# Este script utiliza scikit-learn, seaborn y nltk, de no estar instalado en el sitema 
# correr:
# pip install sklearn seaborn nltk
#
"""

import re
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
#from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import seaborn as sns
import matplotlib.pyplot as plt
#from IPython.display import display
from sklearn import metrics
import os
import joblib
from typing import Pattern, Optional, List, Tuple, Callable


#%%

def leer_archivo(path:str) -> str:
    return open(path,"rt", encoding='utf-8').read()

def leer_stopwords(path:str) -> List[str]:
    with open(path,"rt") as stopwords_file:
        return [stopword for stopword in [stopword.strip().lower() for stopword in stopwords_file] if len(stopword)>0 ]

def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str],List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    if token_regex is None:
        # definicion de que es un token: una letra seguida de letras y numeros
        token_regex = r"[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+"
        token_pattern = re.compile(token_regex)
    return lambda doc: token_pattern.findall(doc)

STOPWORDS_FILE = "stopwords_es.txt"
STOPWORDS_FILE_SIN_ACENTOS = "stopwords_es_sin_acentos.txt"




#%%
#
# Descargo los stopwords con NLTK
#

nltk.download('stopwords')

#%%
#
# Descargo los stopwords con NLTK
#

#DATA_FILE = 'data/balanced-37_5K.csv.gz' # archivo de entrada
DATA_FILE = 'data/small.csv.gz' # archivo de entrada
TEST_SIZE = 0.3               # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

MIN_DF=3
# cantidad maxima de docs que tienen que tener a un token para conservarlo.
MAX_DF=0.8

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2

mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE_SIN_ACENTOS)
mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))
#mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE)+mi_lista_stopwords
#mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))

#%%

#
# Carga del archivo de datos
#

# lectura del CSV en un dataframe
df = pd.read_csv(DATA_FILE, compression='infer')

# genero la columna 'article' como la concatenación de titulo + bajada + contenido del articulo
#text_cols = ['title', 'summary', 'content']
#text_cols = ['title', 'summary', 'content']

#df['article'] = df[text_cols].apply(lambda row: re.sub('[^a-zA-ZáéíóúüñÑ ]+|nan', '', ' '.join(row.values.astype(str))), axis=1)

# genero una columna id para las secciones
df['section_id'] = df['section'].factorize()[0]

# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)



#%%
#
# Procesamiento TF-IDF (conversión a vectores)
#
# Consideramos cada artículo como un "bag of words", es decir consideramos la 
# presencia de las palabras y su frecuencia, pero no su orden.
#

tokenizer = tokenizador()

# creo el vectorizador
tfidf_vect = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS), # uni-gramas y bi-gramas parecen razonables
    lowercase=True,
    stop_words=mi_lista_stopwords,
    tokenizer=tokenizer,
    #stop_words = stopwords.words('spanish')# usamos los stopwords de NLTK
    )

count_vect = CountVectorizer(
    min_df=MIN_DF,                             # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS),                   # uni-gramas y bi-gramas parecen razonables
    stop_words=mi_lista_stopwords, # usamos los stopwords de NLTK
    lowercase=True,
    decode_error='ignore',
    max_df=MAX_DF
)
# procesamos los artículos
#features = tfidf.fit_transform(df.article).toarray()
tfidf_features = tfidf_vect.fit_transform(df.article)
labels = df.section_id
print(tfidf_features.shape)
  # (cantidad de articulos, cantidad de features)
#print(DataFrame(tfidf_features.A,columns=tfidf_vect.get_feature_names()).to_string())

count_vect_features=count_vect.fit_transform(df.article)
print(count_vect_features.shape)
#print(DataFrame(count_vect_features.A,columns=count_vect.get_feature_names()).to_string())

#%%
#
# Usamos Chi^2 para encontrar los términos más correlacionados con cada sección 
# del diario.
#

def imprimir_resumen(seccion, unigramas, bigramas):
    TOP_N = 5

    titulo = "Top %d de la sección '%s'" % (TOP_N, seccion)
    print(titulo)
    print('-' * len(titulo))

    print("Uni-gramas más correlacionados: ")
    for u in unigramas[-TOP_N:]:
        print(" * %s" % u)

    print("\nBi-gramas más correlacionados: ")
    for b in bigramas[-TOP_N:]:
        print(" * %s" % b)

    print("\n")


def encontrar_n_gramas(array, n):
    result = []

    for w in array:
        if w.count(' ') == n - 1:
            result.append(w)

    return result

# obtenemos el top 5 de correlaciones usando TF-IDF primero, luego Count Vectorizer
for features in [tfidf_features, count_vect_features]:
    for section, section_id in sorted(section_to_id.items()):
        features_chi2 = chi2(features, labels == section_id)
        indices = np.argsort(features_chi2[0])
        feature_names = np.array(tfidf_vect.get_feature_names())[indices]

        unigrams = encontrar_n_gramas(feature_names, 1)
        bigrams = encontrar_n_gramas(feature_names, 2)

        imprimir_resumen(section, unigrams, bigrams)

#%%
#
# Generacion de datasets de train y test.
#

X = df['article']
y = df['section']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SIZE, random_state = 42)



#%%
#
# Prueba de modelo Naive Bayes.
#
#naive_b=MultinomialNB()

X_train_count = count_vect.fit_transform(X_train)
X_test_count = count_vect.transform(X_test)

X_train_tfidf = tfidf_vect.fit_transform(X_train)
X_test_tfidf = tfidf_vect.transform(X_test)

#print(DataFrame(X_train_count.A, columns=count_vect.get_feature_names()).to_string())

#%%
### predecir con countvectorizer

# Instantiate a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier_count = MultinomialNB()
# Fit the classifier to the training data
nb_classifier_count.fit(X_train_count, y_train)
# Create the predicted tags: pred
pred_count = nb_classifier_count.predict(X_test_count)
# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test, pred_count)
print(score)
# Calculate the confusion matrix: cm
cm_count = metrics.confusion_matrix(y_test, pred_count)
print(cm_count)

#%%
### predecir con tfidf vectorizer

# Instantiate a Multinomial Naive Bayes classifier: nb_classifier
nb_classifier_tf = MultinomialNB()
# Fit the classifier to the training data
nb_classifier_tf.fit(X_train_tfidf, y_train)
# Create the predicted tags: pred
tfidf_pred = nb_classifier_tf.predict(X_test_tfidf)
# Calculate the accuracy score: score
score = metrics.accuracy_score(y_test, tfidf_pred)
print(score)
# Calculate the confusion matrix: cm
cm_tfid = metrics.confusion_matrix(y_test, tfidf_pred)
print(cm_tfid)


print(nb_classifier_tf.predict(tfidf_vect.transform(["La NASA ha descubierno un nuevo planeta"])))
print(nb_classifier_tf.predict(tfidf_vect.transform(["El dolar blue ha incrementado su precio y la brecha ha llegado al 80 por ciento"])))
print(nb_classifier_tf.predict(tfidf_vect.transform(["El gobernador Axel Kicillof debe lidiar con los problemas de tomas de tierras"])))

print(nb_classifier_count.predict(count_vect.transform(["La NASA ha descubierno un nuevo planeta"])))
print(nb_classifier_count.predict(count_vect.transform(["El dolar blue ha incrementado su precio y la brecha ha llegado al 80 por ciento"])))
print(nb_classifier_count.predict(count_vect.transform(["El gobernador Axel Kicillof debe lidiar con los problemas de tomas de tierras"])))


#%%
#
# Benchmark de modelos:
# Random Forest vs SVM (LinearSVC) vs MultinomialNB (Naive Bayes) vs Logistic 
# Regression.
#

#probando con tfidf:
#features_a_probar=['tfidf_features','count_vect_features']

features=tfidf_features
models = [
    RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0),
    LinearSVC(),
    MultinomialNB(),
    LogisticRegression(random_state=0),
]

CV = 5
cv_df = pd.DataFrame(index=range(CV * len(models)))

entries = []
for model in models:
  model_name = model.__class__.__name__
  accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
  for fold_idx, accuracy in enumerate(accuracies):
    entries.append((model_name, fold_idx, accuracy))

cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])

sns.boxplot(x='model_name', y='accuracy', data=cv_df)
sns.stripplot(x='model_name', y='accuracy', data=cv_df, size=5, jitter=True, edgecolor="gray", linewidth=2)
plt.show()
print(cv_df.sort_values('accuracy',ascending=False).head(1))

#%%
#
# Prueba de modelo SVM
# 

model = LinearSVC()
#model = LogisticRegression(random_state=0)
X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33)
#, random_state=0)

X_train=X_train_tfidf
X_test=X_test_tfidf
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
from sklearn.metrics import confusion_matrix
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d',
            xticklabels=section_id_df.section.values, yticklabels=section_id_df.section.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

score = metrics.accuracy_score(y_test, y_pred)
print(score)
# Calculate the confusion matrix: cm
cm_svc = metrics.confusion_matrix(y_test, y_pred)
print(cm_svc)


#%%
#
# Verificación de errores
#
# Verificamos los errores de cada sección mal clasificada para entender que pasó.
#

def mostrar_errores(predicted, actual, cant_errores, df):
  header = "'%s' predicted as '%s' : %s examples:" % (id_to_section[actual], id_to_section[predicted], conf_mat[actual, predicted])
  print(header)
  print('-' * len(header))
  print(df[['section', 'article']])
  print('')


example_count = 2 # cantidad de errores de clasificación mínimo, para evitar mostrar demasiada data

for predicted in section_id_df.section_id:
  for actual in section_id_df.section_id:
    if predicted != actual and conf_mat[actual, predicted] >= example_count:
      mostrar_errores(
        predicted, 
        actual, 
        conf_mat[actual, predicted], 
        df.loc[indices_test[(y_test == actual) & (y_pred == predicted)]]
      )




#%%
#
# Generamos un reporte de clasificación por cada clase
#
# $Precision=\frac{True Positive}{True Positive + False Positive}$
#
# $Recall=\frac{True Positive}{True Positive + False Negative}$
# 
# $F1=\frac{Precision * Recall}{Precision + Recall}$
# 
# $Accuracy=\frac{True Positives + True Negatives}{True Positives + True Negatives + 
# 
# False Positives + False Negatives}$
# 

print(metrics.classification_report(y_test, y_pred, target_names=df['section'].unique()))



#%%

from tpot import TPOTClassifier
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import numpy as np
from scipy import sparse

#iris = load_iris()
#X_train, X_test, y_train, y_test = train_test_split(iris.data.astype(np.float64),
#    iris.target.astype(np.float64), train_size=0.75, test_size=0.25, random_state=42)

#X_train=sparse.csr_matrix.todense(X_train_tfidf,out=2-D).astype(np.float64)
X_train=X_train_tfidf.A
tpot = TPOTClassifier(generations=5, population_size=50, verbosity=2, random_state=42)
tpot.fit(X_train,y_train)
print(tpot.score(X_test_tfidf.A, y_test))

#%%

#Bayes search

import skopt
print('skopt %s' % skopt.__version__)
from skopt import BayesSearchCV
from sklearn.datasets import load_digits
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold

X, y = load_digits(n_class=10, return_X_y=True)
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.75, test_size=.25, random_state=0)
# log-uniform: understand as search over p = exp(x) by varying x
opt = BayesSearchCV(
SVC(),
{
'C': (1e-6, 1e+6, 'log-uniform'),
'gamma': (1e-6, 1e+1, 'log-uniform'),
'degree': (1, 8), # integer valued parameter
'kernel': ['linear', 'poly', 'rbf'], # categorical parameter
},
n_iter=32,
cv=3
)
opt.fit(X_train, y_train)
#opt.fit(X_train_tfidf.A, y_train)
print("val. score: %s" % opt.best_score_)
print("test score: %s" % opt.score(X_test, y_test))
print("mejores parametros: %s" % opt.best_params_)

params = dict()
params['C'] = (1e-6, 100.0, 'log-uniform')
params['gamma'] = (1e-6, 100.0, 'log-uniform')
params['degree'] = (1,5)
params['kernel'] = ['linear', 'poly', 'rbf', 'sigmoid']

# define evaluation
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
# define the search
search = BayesSearchCV(estimator=SVC(), search_spaces=params, n_jobs=-1, cv=cv)

# perform the search
search.fit(X_train_tfidf.A, y_train)
# report the best result
print(search.best_score_)
print(search.best_params_)






