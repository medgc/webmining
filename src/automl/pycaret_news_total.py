# -*- coding: utf-8 -*-
"""pycaret_news_total.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/10LZLzsklZa7Y7snKtllRGJrt-7SEFrX6

# Clasificador de Noticias Página 12
"""

import re
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
#from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import seaborn as sns
import matplotlib.pyplot as plt
#from IPython.display import display
from sklearn import metrics
import os
import joblib
from typing import Pattern, Optional, List, Tuple, Callable
from pathlib import Path


"""# Funciones"""

def leer_archivo(path:str) -> str:
    return open(path,"rt", encoding='utf-8').read()

def leer_stopwords(path:str) -> List[str]:
    with open(path,"rt") as stopwords_file:
        return [stopword for stopword in [stopword.strip().lower() for stopword in stopwords_file] if len(stopword)>0 ]

def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str],List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    if token_regex is None:
        # definicion de que es un token: una letra seguida de letras y numeros
        token_regex = r"[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+"
        token_pattern = re.compile(token_regex)
    return lambda doc: token_pattern.findall(doc)

#DATA_DIR = os.path.join(Path(__file__).parent.parent, "data")
STOPWORDS_FILE = "stopwords_es.txt"
STOPWORDS_FILE_SIN_ACENTOS = DATA_DIR + "/stopwords_es_sin_acentos.txt"

"""### Carga stopwords"""

nltk.download('stopwords')

"""### Carga datos"""


econ = pd.read_csv(DATA_DIR + '/economia.csv.gz', compression='infer', header=0, sep=',', quotechar='"', error_bad_lines=False)
pais = pd.read_csv(DATA_DIR + '/el-pais.csv.gz', compression='infer', header=0, sep=',', quotechar='"', error_bad_lines=False)
mundo = pd.read_csv(DATA_DIR + '/el-mundo.csv.gz', compression='infer', header=0, sep=',', quotechar='"', error_bad_lines=False)
df = econ.append([pais,mundo]).reset_index(drop=True)
print('Full Dataset',df.shape, 'Categorias',pd.unique(df.section), 'Economia', len(econ), 'Pais', len(pais),
      'Mundo', len(mundo))

df.head()

"""### Seteo parámetros"""

#DATA_FILE = 'output.csv' # directorio de descarga de archivos
TEST_SIZE = 0.3           # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size) 

MIN_DF= 50
MAX_DF= 0.8 # cantidad maxima de docs que tienen que tener a un token para conservarlo.

# numero minimo y maximo de tokens consecutivos que se consideran
MIN_NGRAMS=1
MAX_NGRAMS=2

mi_lista_stopwords = leer_stopwords(STOPWORDS_FILE_SIN_ACENTOS)
mi_lista_stopwords=set (mi_lista_stopwords+stopwords.words('spanish'))

# genero la columna 'article' como la concatenación de titulo + bajada + contenido del articulo
text_cols = ['title', 'summary', 'content']
df['article'] = df[text_cols].apply(lambda row: re.sub('[^a-zA-ZáéíóúüñÑ ]+|nan', '', ' '.join(row.values.astype(str))), axis=1)

# genero una columna id para las secciones
df['section_id'] = df['section'].factorize()[0]

# creo un diccionario para mapear de section_id a section name y vice versa
section_id_df = df[['section', 'section_id']].drop_duplicates().sort_values('section_id')
section_to_id = dict(section_id_df.values)
id_to_section = dict(section_id_df[['section_id', 'section']].values)

tokenizer = tokenizador()

# creo el vectorizador
tfidf_vect = TfidfVectorizer(
    sublinear_tf = True,                    # usamos frecuencia logarítimica
    min_df=MIN_DF,                          # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    norm = 'l2',                            # forzamos vectores de norma 1
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS),   # uni-gramas y bi-gramas parecen razonables
    lowercase=True,
    stop_words=mi_lista_stopwords,
    tokenizer=tokenizer,
    #stop_words = stopwords.words('spanish')# usamos los stopwords de NLTK
    )

count_vect = CountVectorizer(
    min_df=MIN_DF,                          # cantidad de documentos en las que una palabra debe aparecer para tomarse en consideración
    encoding = 'latin-1',                   # debería ser utf-8?
    ngram_range=(MIN_NGRAMS, MAX_NGRAMS),   # uni-gramas y bi-gramas parecen razonables
    stop_words=mi_lista_stopwords,          # usamos los stopwords de NLTK
    lowercase=True,
    decode_error='ignore',
    max_df=MAX_DF
)

"""### Sampleo la data
###### Definicion de las funciones para muestra estratificada
###### Fuente: https://www.kaggle.com/flaviobossolan/stratified-sampling-python
"""

def stratified_sample(df, strata, size=None, seed=None, keep_index= True):
    '''
    It samples data from a pandas dataframe using strata. These functions use
    proportionate stratification:
    n1 = (N1/N) * n
    where:
        - n1 is the sample size of stratum 1
        - N1 is the population size of stratum 1
        - N is the total population size
        - n is the sampling size
    Parameters
    ----------
    :df: pandas dataframe from which data will be sampled.
    :strata: list containing columns that will be used in the stratified sampling.
    :size: sampling size. If not informed, a sampling size will be calculated
        using Cochran adjusted sampling formula:
        cochran_n = (Z**2 * p * q) /e**2
        where:
            - Z is the z-value. In this case we use 1.96 representing 95%
            - p is the estimated proportion of the population which has an
                attribute. In this case we use 0.5
            - q is 1-p
            - e is the margin of error
        This formula is adjusted as follows:
        adjusted_cochran = cochran_n / 1+((cochran_n -1)/N)
        where:
            - cochran_n = result of the previous formula
            - N is the population size
    :seed: sampling seed
    :keep_index: if True, it keeps a column with the original population index indicator
    
    Returns
    -------
    A sampled pandas dataframe based in a set of strata.
    Examples
    --------
    >> df.head()
    	id  sex age city 
    0	123 M   20  XYZ
    1	456 M   25  XYZ
    2	789 M   21  YZX
    3	987 F   40  ZXY
    4	654 M   45  ZXY
    ...
    # This returns a sample stratified by sex and city containing 30% of the size of
    # the original data
    >> stratified = stratified_sample(df=df, strata=['sex', 'city'], size=0.3)
    Requirements
    ------------
    - pandas
    - numpy
    '''
    population = len(df)
    size = __smpl_size(population, size)
    tmp = df[strata]
    tmp['size'] = 1
    tmp_grpd = tmp.groupby(strata).count().reset_index()
    tmp_grpd['samp_size'] = round(size/population * tmp_grpd['size']).astype(int)

    # controlling variable to create the dataframe or append to it
    first = True 
    for i in range(len(tmp_grpd)):
        # query generator for each iteration
        qry=''
        for s in range(len(strata)):
            stratum = strata[s]
            value = tmp_grpd.iloc[i][stratum]
            n = tmp_grpd.iloc[i]['samp_size']

            if type(value) == str:
                value = "'" + str(value) + "'"
            
            if s != len(strata)-1:
                qry = qry + stratum + ' == ' + str(value) +' & '
            else:
                qry = qry + stratum + ' == ' + str(value)
        
        # final dataframe
        if first:
            stratified_df = df.query(qry).sample(n=n, random_state=seed).reset_index(drop=(not keep_index))
            first = False
        else:
            tmp_df = df.query(qry).sample(n=n, random_state=seed).reset_index(drop=(not keep_index))
            stratified_df = stratified_df.append(tmp_df, ignore_index=True)
    
    return stratified_df



def stratified_sample_report(df, strata, size=None):
    '''
    Generates a dataframe reporting the counts in each stratum and the counts
    for the final sampled dataframe.
    Parameters
    ----------
    :df: pandas dataframe from which data will be sampled.
    :strata: list containing columns that will be used in the stratified sampling.
    :size: sampling size. If not informed, a sampling size will be calculated
        using Cochran adjusted sampling formula:
        cochran_n = (Z**2 * p * q) /e**2
        where:
            - Z is the z-value. In this case we use 1.96 representing 95%
            - p is the estimated proportion of the population which has an
                attribute. In this case we use 0.5
            - q is 1-p
            - e is the margin of error
        This formula is adjusted as follows:
        adjusted_cochran = cochran_n / 1+((cochran_n -1)/N)
        where:
            - cochran_n = result of the previous formula
            - N is the population size
    Returns
    -------
    A dataframe reporting the counts in each stratum and the counts
    for the final sampled dataframe.
    '''
    population = len(df)
    size = __smpl_size(population, size)
    tmp = df[strata]
    tmp['size'] = 1
    tmp_grpd = tmp.groupby(strata).count().reset_index()
    tmp_grpd['samp_size'] = round(size/population * tmp_grpd['size']).astype(int)
    return tmp_grpd


def __smpl_size(population, size):
    '''
    A function to compute the sample size. If not informed, a sampling 
    size will be calculated using Cochran adjusted sampling formula:
        cochran_n = (Z**2 * p * q) /e**2
        where:
            - Z is the z-value. In this case we use 1.96 representing 95%
            - p is the estimated proportion of the population which has an
                attribute. In this case we use 0.5
            - q is 1-p
            - e is the margin of error
        This formula is adjusted as follows:
        adjusted_cochran = cochran_n / 1+((cochran_n -1)/N)
        where:
            - cochran_n = result of the previous formula
            - N is the population size
    Parameters
    ----------
        :population: population size
        :size: sample size (default = None)
    Returns
    -------
    Calculated sample size to be used in the functions:
        - stratified_sample
        - stratified_sample_report
    '''
    if size is None:
        cochran_n = round(((1.96)**2 * 0.5 * 0.5)/ 0.02**2)
        n = round(cochran_n/(1+((cochran_n -1) /population)))
    elif size >= 0 and size < 1:
        n = round(population * size)
    elif size < 0:
        raise ValueError('Parameter "size" must be an integer or a proportion between 0 and 0.99.')
    elif size >= 1:
        n = size
    return n

data= stratified_sample(df, ['section'], size=15000, seed=611, keep_index= True).set_index('index')
data.head()

stratified_sample_report(df, ['section'], size=15000)

"""### Creo el set de validación"""

data_unseen = df.drop(data.index)
print('Train Shape', data.shape, 'Validation Shape',data_unseen.shape, "Full database", df.shape)

"""## Creo las features con TF-IDF"""

tfidf_features = tfidf_vect.fit_transform(data.article)
labels = data.section_id

print(tfidf_features.shape, labels.shape, type(tfidf_features), type(labels))

data = pd.concat([pd.DataFrame(tfidf_features.toarray()), labels], axis=1)
data.shape

del  econ, mundo, pais, df

"""# Aplico Auto ML"""


from pycaret.classification import *

py_caret = setup(data = data , target = 'section_id', session_id=611)

compare_models(exclude=['qda', 'lda'], turbo= True)

model = create_model('svm')

tuned = tune_model(model)

plot_model(tuned, plot = 'confusion_matrix')

evaluate_model(tuned)

predict_model(tuned)

final = finalize_model(tuned)
print(final)

save_model(final,'Final SVM')