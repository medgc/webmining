#!/bin/bash

if [ -z "$BASE_DIR" ]
then
  BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  BASE_DIR=`dirname $BIN_DIR`
fi

SCRIPT=`basename $0`
NAME=${SCRIPT%.*}
LOG_FILE="$BASE_DIR/log/$NAME.log"
DEPS='sparse unicode pandas numpy gensim nltk sh scikit-learn tensorflow keras'
PIP_OPTS='--progress-bar off --use-feature=2020-resolver --user'
PYSCRIPT='src/cnn/cnn.py'

mkdir -p $BASE_DIR/log 2>&1 > /dev/null
echo "Logfile: $LOG_FILE"

docker run -ti --rm --name $NAME \
	-w /webmining \
	-v $HOME/.local:/root/.local \
	-v $BASE_DIR:/webmining python:3.8-buster \
	bash -c "export PYTHONPATH=/webmining && pip install $DEPS $PIP_OPTS && python $PYSCRIPT" 2>&1 | tee $LOG_FILE

