#!/bin/bash

if [ -z "$BASE_DIR" ]
then
  BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  BASE_DIR=`dirname $BIN_DIR`
fi
NAME='pyconsole'

CMD=bash

if [ ! -z "$1" ]
then
	CMD="export PYTHONPATH=/webmining && python $1"
fi


docker run -ti --rm --name $NAME \
	-w /webmining \
	-v $HOME/.local:/root/.local \
	-v $BASE_DIR:/webmining python:3.8-buster $CMD

